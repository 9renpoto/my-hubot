cron = require('cron').CronJob

module.exports = (robot) ->
  new cron '0 30 9 * * 1-5', () =>
    robot.send {room: "#general"}, "Good Morning Everyone! :beers:"
  , null, true, "Asia/Tokyo"
